"use strict"

const person = (() => {

	const elPaycheckStat = document.getElementById("status-paycheck")
	const elWalletStat = document.getElementById("status-wallet")
	const elSavingsStat = document.getElementById("status-savings")
	const elLoanStat = document.getElementById("status-loan")
	const elCartStat = document.getElementById("status-cart")

	return {
		paycheck: 0,
		wallet: 0,
		loan: 0,
		savings: 0,
		items: [],
		cart: 0,
		updateStats: function () {
			elPaycheckStat.innerText = this.paycheck.toFixed(2)
			elWalletStat.innerText = this.wallet.toFixed(2)
			elSavingsStat.innerText = this.savings.toFixed(2)
			elLoanStat.innerText = this.loan.toFixed(2)
			elCartStat.innerText = this.cart.toFixed(2)
		},
	}

})()

/**
 * A modal dialog box
 */
function infoBox(title, text, onclose) {
	const msgBox = document.getElementById("msg-box")
	const modal = new bootstrap.Modal(msgBox)
	const modalBody = msgBox.getElementsByClassName("modal-body")[0]

	msgBox.getElementsByClassName("modal-title")[0].innerText = title

	const p = document.createElement('p')
	p.innerText = text
	modalBody.replaceChildren(p)

	const listener = () => {
		msgBox.removeEventListener('hidden.bs.modal', listener)
		onclose()
	}
	msgBox.addEventListener('hidden.bs.modal', listener)
	msgBox.querySelector(".modal-footer button").onclick = (ev) => modal.hide()

	modal.show(true)
}


const Card = (() => {
	return Object.freeze({
		createEmptyCard: function () {
			const div = document.createElement('div')
			div.innerHTML = '' +
				'<div class="col">' +
				'	<div class="card mb-4 rounded-3 shadow-sm">' +
				'	</div>' +
				'</div>' +
				''.replace("\t", '') // remove tab
			return div
		},

		createImageCard: function (url, alt) {
			const card = this.createEmptyCard()
			const cardMain = card.getElementsByClassName('card')[0]
			cardMain.innerHTML = `<img src="${url}" alt="${alt}" style="width: 100%;">`
			return card
		},

		createElementWithText: function (tag, text) {
			const e = document.createElement(tag)
			e.innerText = text
			return e
		},

		createElementWithList: function (listtag, list) {
			const l = document.createElement(listtag)
			for (let i = 0; i < list.length; i++) {
				l.appendChild(this.createElementWithText('li', list[i]))
			}
			return l
		},

		createInfoCard: function (titletag, title, infotag, info) {
			const card = this.createEmptyCard()
			const cardMain = card.getElementsByClassName('card')[0]
			cardMain.innerHTML = '' +
				'<div class="card-header py-3">' +
				'</div>' +
				'<div class="card-body">' +
				'</div>' +
				''
			const header = cardMain.getElementsByClassName('card-header')[0]
			header.appendChild(this.createElementWithText(titletag, title))

			const body = cardMain.getElementsByClassName('card-body')[0]
			if (typeof info === 'string') {
				body.appendChild(this.createElementWithText(infotag, info))
			}
			else if (typeof info === 'object' && (infotag === 'ul' || infotag === 'ol')) {
				body.appendChild(this.createElementWithList(infotag, info))
			}

			return card
		},

		createGeneralCard: function (headerElememt, bodyElement) {
			const card = this.createEmptyCard()
			const cardMain = card.getElementsByClassName('card')[0]
			cardMain.innerHTML = '' +
				'<div class="card-header py-3">' +
				'</div>' +
				'<div class="card-body">' +
				'</div>' +
				''
			const header = cardMain.getElementsByClassName('card-header')[0]
			header.appendChild(headerElememt)

			const body = cardMain.getElementsByClassName('card-body')[0]
			body.appendChild(bodyElement)
			return card
		},

	})
})()

const places = (() => {
	/* private variables */

	/*
	 * **Watch out**
	 * Indexing must be the same for `sections` and `titles`
	 * and correspond to the values given in the returned object.
	 */
	const places = [
		document.getElementById("mainmenu"),
		document.getElementById("workplace"),
		document.getElementById("bank"),
		document.getElementById("shop"),
	]

	const titles = ["Where to go?", "At Work", "The Bank", "The Komputer Shop"]

	const subtitle = document.getElementById("sub-title")

	return Object.freeze({
		MAIN_MENU: 0,
		WORKPLACE: 1,
		BANK: 2,
		SHOP: 3,

		displaySection: function (id) {
			const s = places

			if (typeof (id) !== "number") return
			if (id < 0 || id >= s.length) return

			for (let i = 0; i < s.length; i++)
				s[i].style.display = "none"
			s[id].style.display = ""
			subtitle.innerText = titles[id]
		}
	})
})()


/*
* Workplace GUI
*/
const wpGUI = (() => {
	const img = document.getElementById("img-work")
	const btnWork = document.getElementById("btn-work")
	const btnEndWork = document.getElementById("btn-end-work")

	// init
	function init() {
		img.src = "img/Idle2.png"
		btnWork.style.display = ""
		btnEndWork.style.display = ""
	}

	// callback variables
	let onDoWork = () => { }
	let onEndWork = () => { }

	btnWork.onclick = () => {
		btnWork.disabled = true
		btnEndWork.disabled = true
		img.src = "img/StartWorking2.gif"
		setTimeout(() => {
			onDoWork()
			img.src = "img/Idle2.png"
			btnWork.disabled = false
			btnEndWork.disabled = false
		}, 6000)
	}

	btnEndWork.onclick = () => {
		btnWork.style.display = "none"
		btnEndWork.style.display = "none"
		img.src = "img/Done Working.gif"
		onEndWork()
	}

	const obj = {
		// functions
		init: init,

		// event handlers
		set onDoWork(handler) { onDoWork = handler },
		set onEndWork(handler) { onEndWork = handler }
	}

	return Object.freeze(obj)
})()

/*
 * Workplace logic
 */
const work = (() => {

	const PAYCHECK_INCREMENT = 100

	function init() {
		wpGUI.init()
	}

	wpGUI.onDoWork = () => {
		person.paycheck += PAYCHECK_INCREMENT
		person.updateStats()
	}

	wpGUI.onEndWork = () => {

		let downpayment = 0
		let paycheck_orig = person.paycheck

		if (person.loan > 0) {
			downpayment = person.paycheck / 10

			if (downpayment > person.loan)
				downpayment = person.loan

			person.paycheck -= downpayment
		}

		/* Visual feedback - popup box? */
		let msg = `You received paycheck of ${paycheck_orig.toFixed(2)}.`
		if (person.loan > 0)
			msg += `${downpayment.toFixed(2)} is used to pay down your loan.`

		infoBox("Payment", msg, () => {
			wpGUI.init()
			places.displaySection(places.MAIN_MENU)

			person.loan -= downpayment
			person.paycheck -= downpayment
			person.wallet += person.paycheck
			person.paycheck = 0

			person.updateStats()
		})

	}

	return Object.freeze(
		{
			init: init,
		}
	)
})()

/**
 *  Bank GUI
 */
const bankGUI = (() => {
	// bank elements
	const dspSave = document.getElementById("bank-save-money")
	const dspApply = document.getElementById("bank-apply-for-loan")
	const dspRepay = document.getElementById("bank-pay-down-loan")

	const btnSave = document.querySelector("#bank-save-money button")
	const btnApply = document.querySelector("#bank-apply-for-loan button")
	const btnRepay = document.querySelector("#bank-pay-down-loan button")

	const btnExit = document.getElementById("bank-exit")

	let onSave = () => { }
	let onApply = () => { }
	let onRepay = () => { }
	let onExit = () => { }

	btnSave.onclick = () => onSave()
	btnApply.onclick = () => onApply()
	btnRepay.onclick = () => onRepay()
	btnExit.onclick = () => onExit()

	return Object.freeze({
		BTN_SAVE: 1,
		BTN_APPLY: 2,
		BTN_REPAY: 4,

		set onSave(handler) { onSave = handler },
		set onApply(handler) { onApply = handler },
		set onRepay(handler) { onRepay = handler },
		set onExit(handler) { onExit = handler },

		displayButtons: function (buttons) {
			dspSave.style = (buttons & this.BTN_SAVE) ? "" : "display: none !important;"
			dspApply.style = (buttons & this.BTN_APPLY) ? "" : "display: none !important;"
			dspRepay.style = (buttons & this.BTN_REPAY) ? "" : "display: none !important;"
		},

		dialogLoanRequest: function (title, text, validator, onclose) {
			// Uses the modal message box (id = "msg-box").
			// Only one modal box is allowed.
			const msgBox = document.getElementById("msg-box")
			const modalBody = msgBox.getElementsByClassName("modal-body")[0]
			const modal = new bootstrap.Modal(msgBox)

			msgBox.querySelector(".modal-title").innerText = title

			const p = document.createElement('p')
			p.innerText = text
			modalBody.replaceChildren(p)

			const form = document.createElement("form")
			form.innerHTML =
				'<div class="form-group mb-2">' +
				'	<label for="loanSum">Sum</label> ' +
				'	<input type="number" min="0" style=""' +
				'		class="form-control-plaintext form-loan-input" ' +
				'		placeholder="The wanted size of the loan." >' +
				'</div>' +
				'<button type="button" class="btn btn-primary mb-2 form-submit">' +
				'	Apply</button>' +
				'<div class="form-result">&nbsp;</div>' +
				''
			const result = form.getElementsByClassName('form-result')[0]
			const submit = form.getElementsByClassName('form-submit')[0]
			const loanSum = form.getElementsByTagName('input')[0]

			submit.onclick = () => {
				let accepted = false
				let response = "";
				accepted, response = validator(parseFloat(loanSum.value));
				result.innerText = response
				if (accepted) {
				}
			}
			modalBody.appendChild(form)

			const listener = () => {
				msgBox.removeEventListener('hidden.bs.modal', listener)
				onclose()
			}
			msgBox.addEventListener('hidden.bs.modal', listener)
			msgBox.querySelector(".modal-footer button").onclick = (ev) => modal.hide()

			modal.show(true)
		}
	})

})()

/*
 * Bank logic
 */
const bank = (() => {
	function init() {
		let btns = bankGUI.BTN_SAVE
		btns |= (person.loan === 0) ? bankGUI.BTN_APPLY : 0
		btns |= (person.loan > 0) ? bankGUI.BTN_REPAY : 0
		bankGUI.displayButtons(btns)
	}

	bankGUI.onSave = () => {
		person.savings += person.wallet;
		person.wallet = 0
		person.updateStats()
	}

	bankGUI.onApply = () => {

		if (person.loan > 0) {
			console.error("Cannot apply for loan: loan = ",
				person.loan, ". Programming error.")
			return
		}
		let loanLimit = person.savings * 2
		let loan = 0
		bankGUI.dialogLoanRequest(
			"Apply for loan",
			`You can apply for a loan of maximum ${loanLimit.toFixed(2)}.`,

			(value) => {
				// Shall we accept the loan?
				console.assert(typeof value === "number")

				loan = value
				let accepted = loan <= loanLimit
				if (loan < 0)
					loan = 0
				if (loan === 0)
					return true, "It's OK. You don't have to have a loan, you know."

				if (accepted)
					return true, "Congratulations, " +
						`You have been granted a loan that amounts to ${loan.toFixed(2)}.`

				loan = 0
				return false, "Your application has been denied."
			},

			() => {
				// Dialog closed. Update values:
				person.loan += loan
				person.savings += loan
				person.updateStats()
				init()
			}
		)
	}

	bankGUI.onRepay = () => {

		let allMoney = person.savings + person.wallet
		let maxDownpayment = allMoney < person.loan ?
			allMoney : person.loan
		person.loan -= maxDownpayment
		person.savings -= maxDownpayment

		if (person.savings < 0) {
			person.wallet -= person.savings
			person.savings = 0
		}

		person.updateStats()
		init()
	}

	bankGUI.onExit = () => places.displaySection(places.MAIN_MENU)

	return Object.freeze({
		init: init,
	})
})()

/*
 * Shop init
 */
const shopGUI = (() => {

	const sectShop = document.getElementById("shop")
	const sidebar = sectShop.getElementsByTagName("aside")[0]
	const btnExit = sidebar.getElementsByTagName("button")[0]
	const lstKomputers = sidebar.getElementsByTagName("ul")[0]

	let _computers = [] // to set the type to array instead of any.
	let _baseUrl = ""

	let onExit = () => { }
	let onSeletMerchendice = (id) => { }

	btnExit.onclick = () => { onExit() }

	function getComputerById(id) {
		for (let i = 0; i < _computers.length; i++) {
			if (_computers[i].id === id)
				return _computers[i]
		}
		return null
	}

	function setupProductDisplay() {
		const base = document.createElement('div')
		for (let i = 0, count = 0; i < _computers.length; i++) {
			if (count == 3)
				break
			if (_computers.stock === 0)
				continue
			count++

			let img = document.createElement('img')
			img.setAttribute('src', _baseUrl + _computers[i].image)
			img.setAttribute('alt', _computers[i].title)
			img.style.maxWidth = '100%'
			img.style.maxHeight = '100px'

			let body = document.createElement('div')
			body.appendChild(Card.createElementWithText('h2', _computers[i].title))
			body.appendChild(Card.createElementWithText('p', _computers[i].description))
			body.appendChild(Card.createElementWithText('p', _computers[i].price.toFixed(2)))

			let card = Card.createGeneralCard(img, body)
			card.onclick = () => onSeletMerchendice(computers[i].id)
			base.appendChild(card)
		}
		const productCardsContainer = sectShop.getElementsByClassName('row')[0]
		productCardsContainer.innerHTML = base.innerHTML
		const sideMenu = document.createElement('col')
		sideMenu.appendChild(sidebar)
		productCardsContainer.prepend(sideMenu)
	}

	function setupSidebar() {
		const list = []
		for (let i = 0; i < _computers.length; i++) {
			list.push(_computers[i].title)
		}

		const ulComputers = Card.createElementWithList('ul', list)
		const liComputers = ulComputers.getElementsByTagName('li')
		for (let i = 0; i < liComputers.length; i++) {
			liComputers[i].classList.add("nav-item")
			liComputers[i].onclick = () => onSeletMerchendice(_computers[i].id)
		}
		lstKomputers.innerHTML = ulComputers.innerHTML
	}

	return Object.freeze({
		init: function (computers, baseUrl) {
			_computers = computers
			_baseUrl = baseUrl

			setupSidebar()

			setupProductDisplay()
		},

		set onExit(handler) { onExit = handler },
		set onSelectMerchendice(handler) { onSeletMerchendice = handler },

		displayDetailsForm: function (title, id, onclose) {
			const msgBox = document.getElementById("msg-box")
			const modal = new bootstrap.Modal(msgBox)
			const modalBody = msgBox.getElementsByClassName("modal-body")[0]

			const computer = getComputerById(id)
			console.assert(computer !== null)

			msgBox.getElementsByClassName("modal-title")[0].innerText = title

			modalBody.appendChild(Card.createImageCard(url, computer.title))
			modalBody.appendChild(Card.createInfoCard('h2', computer.title, 'h3', computer.price))

			const details = Card.createInfoCard('p', computer.description, 'ul', computer.specs)
			details.style.width = "100% !important"
			modalBody.appendChild(details)

			const listener = () => {
				msgBox.removeEventListener('hidden.bs.modal', listener)
				onclose()
			}
			msgBox.addEventListener('hidden.bs.modal', listener)
			msgBox.querySelector(".modal-footer button").onclick = (ev) => modal.hide()

			modal.show(true)
		},
	})
})()

const shop = (() => {
	async function getJsonObjectFileFromNet(url, onSuccess, onError) {
		try {
			const response = await fetch(url)
			const jsonObj = await response.json()
			setTimeout(() => onSuccess(jsonObj) , 1); // exits the try statement.
		} catch (e) {
			onError(e)
		}
	}

	let computers = null

	let initialized = false

	return Object.freeze({
		init: function () {
			if (!initialized) {
				getJsonObjectFileFromNet('https://noroff-komputer-store-api.herokuapp.com/computers',
					(value) => {
						computers = value;
						shopGUI.init(computers,
							'https://noroff-komputer-store-api.herokuapp.com/')
					},
					(e) => { console.log(e) })
				shopGUI.onExit = () => {
					places.displaySection(places.MAIN_MENU)
				}

				shopGUI.onSelectMerchendice = () => {

				}
				initialized = true
			}
		},
	})
})()



/*
 * Main Menu init (can this be moved into the places "class" ?)
 */
document.getElementById("btn-go-to-work").onclick = () => {
	wpGUI.init()
	places.displaySection(places.WORKPLACE)
}

document.getElementById("btn-go-to-the-bank").onclick = () => {
	bank.init()
	places.displaySection(places.BANK)
}

document.getElementById("btn-go-shopping").onclick = () => {
	shop.init()
	places.displaySection(places.SHOP)
}

/*
 * Startup
 */
places.displaySection(places.MAIN_MENU)
// places.displaySection(places.BANK) // for debug
