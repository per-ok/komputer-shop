# Follow the Money, Honey

```mermaid
graph
P(Pay Check)
BS(Bank - Savings)
BL(Bank - Loan)
S(Shop)
P -- Repay Loan --> BL
BS -- Repay Loan --> BL
P -- Deposit --> BS
BL -- Lend --> BS
BS -- Buy --> S
```
